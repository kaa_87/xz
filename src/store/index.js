import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)


export const store = new Vuex.Store({
  state: {
    number_attempts: 3,
    left_attempts: null
  },
  getters : {
    get_number_attempts : state => {
      return state.number_attempts
    },
    get_left_attempts : state => {
      return state.left_attempts
    }
  },
  mutations: {
    reduce_left_attempts : state => {
      state.left_attempts--
    },
    set_default_number_attempts : state => {
      state.left_attempts = state.number_attempts
    }
  },
  actions : {}
})