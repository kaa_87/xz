const Home = () => import(/* webpackChunkName: "Home" */ "@/pages/Home")
const Numbers = () => import(/* webpackChunkName: "Numbers" */ "@/pages/Numbers")
const Alphabet = () => import(/* webpackChunkName: "Alphabet" */ "@/pages/Alphabet")

const routes = [
    {
      path: "",
      name: "home",
      component: Home,
    },
    {
      path: "/numbers",
      name: "numbers",
      component: Numbers,
    },
    {
      path: "/alphabet",
      name: "alphabet",
      component: Alphabet,
    },
    {
      path: "/*",
      component: { render: (h) => h("div", ["404! Page Not Found!"]) }
    }
]

export default routes